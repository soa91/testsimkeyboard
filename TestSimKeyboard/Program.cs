﻿using System;
using KeyboardLibrary;

namespace TestSimKeyboard
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("\tpress - press 2 x Ctrl\n\texit - close app\n\n");
            string str = "";
            while (!str.Equals("exit"))
            {
                str = Console.ReadLine();

                if (string.IsNullOrWhiteSpace(str)) continue;
                if (str.ToLower().Equals("press"))
                {
                    KeyboardSimulator.KeyPressB((byte)17);
                    KeyboardSimulator.KeyPressB((byte)17);
                }
            }
        }
    }
}