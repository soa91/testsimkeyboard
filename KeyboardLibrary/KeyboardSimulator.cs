﻿using System.Runtime.InteropServices;

namespace KeyboardLibrary
{
    public static class KeyboardSimulator
    {
        const int KEYEVENTF_KEYUP = 0x2;

        [DllImport("user32.dll")]
        static extern void keybd_event(byte key, byte scan, int flags, int extraInfo);

        private static void KeyDownB(byte keyCode)
        {
            keybd_event(keyCode, 0, 0, 0);
        }

        private static void KeyUpB(byte keyCode)
        {
            keybd_event(keyCode, 0, KEYEVENTF_KEYUP, 0);
        }

        public static void KeyPressB(byte keyCode)
        {
            KeyDownB(keyCode);
            KeyUpB(keyCode);
        }
    }
}
